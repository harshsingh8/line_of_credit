class AddCreditLineRefToTransactions < ActiveRecord::Migration
  def change
    add_reference :transactions, :credit_line, index: true, foreign_key: true
  end
end
