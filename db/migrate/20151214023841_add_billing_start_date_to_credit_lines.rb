class AddBillingStartDateToCreditLines < ActiveRecord::Migration
  def change
    add_column :credit_lines, :billing_start_date, :date
  end
end
