class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.decimal :amount
      t.string :type
      t.boolean :valid

      t.timestamps null: false
    end
  end
end
