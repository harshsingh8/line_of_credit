class CreateCreditLines < ActiveRecord::Migration
  def change
    create_table :credit_lines do |t|
      t.references :user, index: true, foreign_key: true
      t.decimal :credit_limit
      t.decimal :apr
      t.decimal :balance
      t.decimal :accrued_interest
      t.decimal :remaining_credit

      t.timestamps null: false
    end
  end
end
