class RenameColumnValidinTransactiontoGood < ActiveRecord::Migration
  def change
    rename_column :transactions, :valid, :good
  end
end
