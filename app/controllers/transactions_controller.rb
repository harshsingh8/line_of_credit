class TransactionsController < ApplicationController
  before_filter :authorize
  def index
    @transactions = Transaction.where(credit_line_id: params[:credit_line_id])
  end
end
