class CreditLinesController < ApplicationController
  before_filter :authorize

  def show
    @user = current_user
    @credit_line = @user.credit_line
  end

  def new
    @user = current_user
    @credit_line = CreditLine.new
  end

  def create
    @user = current_user
    @credit_line = CreditLine.new(credit_line_params)
    @credit_line.remaining_credit = @credit_line.credit_limit
    @credit_line.billing_start_date = Date.today #Date.today - 30.days
    if @credit_line.save
      flash[:success] = "Credit Line Created"
      redirect_to user_path(@credit_line.user)
    else
      flash[:alert] = "Failure"
      redirect_to user_path(@user.id)
    end
  end
  
  def destroy
    CreditLine.find(params[:credit_line_id]).destroy
    flash[:success] = "Credit Line deleted"
    redirect_to user_path(current_user)
  end


  def transactions_form
    @user = current_user
    @credit_line = @user.credit_line
    @category = params[:category]
    @transaction = Transaction.new
  end

  def draw
    current_t_date = params[:transaction][:transaction_date]
    CreditLine.get_accrued_interest(current_user.credit_line,current_t_date)
    @transaction = CreditLine.create_transaction(transaction_params)
    if @transaction
      @credit_line = CreditLine.find(@transaction.credit_line_id)
      if @credit_line && @transaction.amount <= @credit_line.remaining_credit
        @credit_line.balance += @transaction.amount
        @credit_line.remaining_credit -= @transaction.amount
        @credit_line.save
        @transaction.update_attribute(:good, true)
        flash[:notice] = "#{@transaction.amount} withdrawn, remaining credit: #{@credit_line.remaining_credit}"
        redirect_to user_path(current_user)
      else
        @transaction.update_attribute(:good, false)
        flash[:notice] = "Transaction rejected because it exceeds credit limit"
        redirect_to user_path(@credit_line.user)
      end
    else
      flash[:notice] = "Transaction not created"
      redirect_to user_path(current_user)
    end

  end

  def pay
    current_t_date = params[:transaction][:transaction_date]
    CreditLine.get_accrued_interest(current_user.credit_line,current_t_date)
    @transaction = CreditLine.create_transaction(transaction_params)
    if @transaction
      @credit_line = CreditLine.find(@transaction.credit_line_id)
      if @transaction.amount > @credit_line.balance
        flash[:alert] = "Your payment is greater than the balance"
        @transaction.update_attribute(:good, false)
        redirect_to user_path(@credit_line.user)
      else
        @credit_line.balance -= @transaction.amount
        @credit_line.remaining_credit += @transaction.amount
        if @credit_line.save
          @transaction.update_attribute(:good, true)
          flash[:notice] = "#{@transaction.amount} paid, remaining credit: #{@credit_line.remaining_credit}"
          redirect_to user_path(current_user)
        else
          @transaction.update_attribute(:good, false)
          flash[:notice] = "Transaction rejected"
          redirect_to user_path(@credit_line.user)
        end
      end
    else
      flash[:notice] = "Transaction not created"
      redirect_to user_path(current_user)
    end
  end

  def get_monthly_bill
    @user = current_user
    @credit_line = @user.credit_line
    current_t_date = @credit_line.billing_start_date + 30.days
    CreditLine.apply_monthly_interest(@credit_line,current_t_date)
    @credit_line.reload
    flash[:notice] = "Total bill for this month: #{@credit_line.balance.round(2)}"
    redirect_to user_path(current_user)
  end
  
  def move_back_date
    @user = current_user
    @credit_line = @user.credit_line
    @credit_line.billing_start_date = @credit_line.billing_start_date - 30.days
    @credit_line.save
    flash[:notice] = "Billing start date moved 30 days back"
    redirect_to user_path(current_user)
  end

  private
    def credit_line_params
      params.require(:credit_line).permit(:user_id, :credit_limit, :apr)
    end

    def transaction_params
      params.require(:transaction).permit(:credit_line_id, :amount, :category, :transaction_date)
    end
  
end
