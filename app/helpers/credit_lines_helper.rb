module CreditLinesHelper
  def calculate_end_date(date)
    (date + 1.month).strftime("%m/%d/%Y")
  end
end
