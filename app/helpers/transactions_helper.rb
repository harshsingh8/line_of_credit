module TransactionsHelper
  def transaction_type(category)
    case category
    when 'draw'
      'credit'
    when 'pay'
      'debit'
    when 'interest'
      'interest'
    else
      'undefined'
    end
  end

  def status(value)
    return value ? 'passed' : 'failed'
  end
end
