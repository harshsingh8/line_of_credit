class User < ActiveRecord::Base
  has_secure_password
  has_one :credit_line, dependent: :destroy
end
