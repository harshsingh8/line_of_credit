class Transaction < ActiveRecord::Base
  belongs_to :credit_line
  validates :credit_line_id, presence: true
  validates :category, presence: true
  validates :amount, presence: true,
                     numericality: { greater_than_or_equal_to: 0 }
  validates :transaction_date, presence: true

  scope :latest, -> { order("transaction_date DESC").first }
end
