class CreditLine < ActiveRecord::Base
  belongs_to :user
  has_many :transactions
  before_save :default_values

  validates :user_id, presence: true
  validates :credit_limit, presence: true,
                           numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 2000 }
  validates :apr, presence: true,
                            numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 50.00 }


  def check_interest_payment_day?
    return Date.today - self.billing_start_date >= 30
  end

  def self.apply_monthly_interest(credit_line, current_t_date)
    if credit_line.check_interest_payment_day?
      CreditLine.get_accrued_interest(credit_line, current_t_date)
      transaction = CreditLine.create_transaction({credit_line_id: credit_line.id,
                    amount: credit_line.accrued_interest, category: 'interest', transaction_date: current_t_date.to_date })
      transaction.update_attribute(:good, true)
      credit_line.balance += credit_line.accrued_interest
      credit_line.accrued_interest = 0
      credit_line.billing_start_date = Date.today
      credit_line.remaining_credit = credit_line.credit_limit
      credit_line.save
    end
  end

  def self.get_accrued_interest(credit_line,current_t_date)
    previous_transaction = Transaction.where(credit_line_id: credit_line.id).latest
    if previous_transaction.present?
      no_of_days = (current_t_date.to_date - previous_transaction.transaction_date).to_i
      credit_line.accrued_interest = credit_line.accrued_interest + credit_line.balance * ((credit_line.apr/100) / 365) * no_of_days
      credit_line.save
    end
  end

  def self.create_transaction(transaction_params)
    transaction = Transaction.new(transaction_params)
    transaction.save
    transaction
  end

  private
    def default_values
      self.balance ||= 0.0
      self.accrued_interest ||= 0.0
      self.remaining_credit ||= 0.0
    end

end
